import React, { useState, useEffect } from 'react';

import './Home.scss';

import Header from '../../components/Header';

const Home: React.FC = (): JSX.Element => {
  return (
    <section className="home">
      <Header />
      <div className="container">
        <h1>Home page</h1>
      </div>
    </section>
  );
}

export default Home;
