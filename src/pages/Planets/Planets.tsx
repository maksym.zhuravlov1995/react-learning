import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import './Planets.scss';

import { PlanetType } from '../../components/Planet/types';
import { fetchPlanets } from '../../store/actions/planets/planetsActions';

import Planet from '../../components/Planet';
import Header from '../../components/Header';

const Planets: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const data: PlanetType[] = useSelector((state: any) => state.planets.planets);

  useEffect(() => {
    dispatch(fetchPlanets());
  }, []);

  return (
    <section className="planets">
      <Header />
      <div className="container">
        <h1 className="planets__title">Planets</h1>
        <div className="planets__list">
          {data.map((item: PlanetType) => (
            <Planet planet={item} />
          ))}
        </div>
      </div>
    </section>
  );
}

export default Planets;
