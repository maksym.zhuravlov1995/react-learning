export interface SignupForm {
  [key: string]: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  repeatPassword: string;
}

export interface SignupFormErrors {
  [key: string]: any;
  firstName: boolean;
  email: boolean;
  password: boolean;
  repeatPassword: {
    isEmpty: boolean;
    isInvalid: boolean;
  }
}
