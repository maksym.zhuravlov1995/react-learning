import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import './Signup.scss';

import { configInputs } from './config';

import { logIn } from '../../store/actions/login/loginActions';

import Form from '../../components/Form';

const Signup: React.FC = (): JSX.Element => {
  const [ isPopupOpen, setIsPopupOpen ] = useState(false);
  const [ isPopupWaiting, setIsPopupWaiting ] = useState(false);

  const dispatch = useDispatch();
  const history = useHistory();

  const sendDataForm = (formValues: any, isFormValid: boolean) => {
    if (isFormValid) {
      localStorage.setItem('form-values', JSON.stringify(formValues));

      dispatch(logIn());
      history.push('/');
    }
  };

  return (
    <>
    <section className="signup">
      <div className="container">
        <Form
          configInputs={configInputs}
          className="signup"
          isPopupWaiting={isPopupWaiting}
          sendDataForm={sendDataForm}
        />
      </div>
    </section>
    {/* {isPopupOpen && (
      <Modal>
        <div className="signup__popup">
          <h1>
            You have signed up successfully.
          </h1>
          <button
            className="signup__popup-close"
            type="button"
            onClick={() => setIsPopupOpen(false)}
          >
            X
          </button>
        </div>
      </Modal>
    )} */}
    </>
  );
}

export default Signup;
