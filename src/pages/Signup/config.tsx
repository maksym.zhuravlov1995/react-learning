export const configInputs = [
  {
    label: "First name",
    type: "text",
    id: "first-name",
    name: "firstName",
    errorMessage: "The first name must not be empty.",
    isRequired: true
  },
  {
    label: "Last name",
    type: "text",
    id: "last-name",
    name: "lastName",
    errorMessage: null,
    isRequired: false
  },
  {
    label: "Email",
    type: "email",
    id: "email",
    name: "email",
    errorMessage: "The email must not be empty.",
    isRequired: true
  },
  {
    label: "Password",
    type: "password",
    id: "password",
    name: "password",
    errorMessage: "Please enter your password.",
    isRequired: true
  },
  {
    label: "Repeat password",
    type: "password",
    id: "repeat-password",
    name: "repeatPassword",
    errorMessage: "Please repeat your password.",
    isRequired: true
  },
];
