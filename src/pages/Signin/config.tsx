export const configInputs = [
  {
    label: "Email",
    type: "email",
    id: "email",
    name: "email",
    errorMessage: "The email must not be empty.",
    isRequired: true
  },
  {
    label: "Password",
    type: "password",
    id: "password",
    name: "password",
    errorMessage: "Please enter your password.",
    isRequired: true
  }
];
