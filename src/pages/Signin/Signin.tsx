import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import './Signin.scss';

import { configInputs } from './config';

import { logIn } from '../../store/actions/login/loginActions';

import Modal from '../../components/Modal';
import Form from '../../components/Form';

const Signin: React.FC = (): JSX.Element => {
  const [ isPopupOpen, setIsPopupOpen ] = useState(false);
  const [ isPopupWaiting, setIsPopupWaiting ] = useState(false);

  const dispatch = useDispatch();

  const sendDataForm = (formValues: any, isFormValid: boolean) => {
    if (isFormValid) {
      setIsPopupWaiting(true);
      localStorage.setItem('form-values', JSON.stringify(formValues));

      setTimeout(() => {
        setIsPopupOpen(true);
        setIsPopupWaiting(false);
        dispatch(logIn());
      }, 5000);
    }
  };

  return (
    <>
    <section className="login">
      <div className="container">
        <Form
          configInputs={configInputs}
          className="login"
          isPopupWaiting={isPopupWaiting}
          sendDataForm={sendDataForm}
        />
      </div>
    </section>
    {isPopupOpen && (
      <Modal>
        <div className="signup__popup">
          <h1>
            You have signed in successfully.
          </h1>
          <button
            className="signup__popup-close"
            type="button"
            onClick={() => setIsPopupOpen(false)}
          >
            X
          </button>
        </div>
      </Modal>
    )}
    </>
  );
}

export default Signin;
