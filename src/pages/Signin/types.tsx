export interface LoginForm {
  [key: string]: string;
  email: string;
  password: string;
}

export interface LoginFormErrors {
  [key: string]: any;
  email: boolean;
  password: boolean;
}
