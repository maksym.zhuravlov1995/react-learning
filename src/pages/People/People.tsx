import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import './People.scss';

import { PersonType } from '../../components/Person/types';
import { fetchPeople } from '../../store/actions/people/peopleActions';

import Person from '../../components/Person';
import Header from '../../components/Header';

const People: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const data: PersonType[] = useSelector((state: any) => state.people.people);

  useEffect(() => {
    dispatch(fetchPeople());
  }, []);

  return (
    <section className="people">
      <Header />
      <div className="container">
        <h1 className="people__title">People</h1>
        <div className="people__list">
          {data.map((item: PersonType) => (
            <Person person={item} />
          ))}
        </div>
      </div>
    </section>
  );
}

export default People;
