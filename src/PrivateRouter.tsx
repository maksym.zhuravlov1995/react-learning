import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

interface Props {
  [propName: string]: any;
}

const PrivateRouter: React.FC<Props> = ({ component: Component, ...rest }): JSX.Element => {
  const isLogged: boolean = useSelector((state: any) => state.login.isLoggedIn);

  return (
    <Route
    {...rest}
      render={props => isLogged ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/signup',
            state: { from: props.location }
          }}
        />
      )}
    />
  );
}

export default PrivateRouter;
