export const fetchPeople = () => {
  return async (dispatch: any, getState: any) => {
    const response: any = await fetch('https://swapi.dev/api/people');
    const data: any = await response.json();
    const results = data.results;

    dispatch({
      type: 'FETCH_PEOPLE',
      results
    });
  };
};
