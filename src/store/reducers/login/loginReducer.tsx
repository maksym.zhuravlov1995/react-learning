const initialState = {
  isLoggedIn: false
};

export const loginReducer = (state: any = initialState, action: any) => {
  if (action.type === 'LOGIN') {
    return {
      ...state,
      isLoggedIn: true
    };
  }

  return state;
};
