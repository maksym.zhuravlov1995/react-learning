const initialState = {
  people: []
};

export const peopleReducer = (state: any = initialState, action: any) => {
  if (action.type === 'FETCH_PEOPLE') {
    return {
      ...state,
      people: action.results
    };
  }

  return state;
}
