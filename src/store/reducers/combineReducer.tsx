import { combineReducers } from 'redux';
import { peopleReducer } from './people/peopleReducer';
import { planetsReducer } from './planets/planetsReducer';
import { loginReducer } from './login/loginReducer';

export const combineReducer: any = combineReducers({
  people: peopleReducer,
  planets: planetsReducer,
  login: loginReducer
});
