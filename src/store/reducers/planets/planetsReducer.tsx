const initialState = {
  planets: []
};

export const planetsReducer = (state: any = initialState, action: any) => {
  if (action.type === 'FETCH_PLANETS') {
    return {
      ...state,
      planets: action.results
    };
  }

  return state;
}
