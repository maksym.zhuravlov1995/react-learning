import React from 'react';

import './Person.scss';

import { Props } from './Props';

const Person: React.FC<Props> = ({ person }): JSX.Element => {
  return (
    <div className="person" key={person.name}>
      <p className="person__name">{person.name} ({person.gender}, {person.birth_year})</p>
      <p>Height: {person.height} cm</p>
      <p>Mass: {person.mass} kg</p>
      <p>Hair color: {person.hair_color}</p>
      <p>Skin color: {person.skin_color}</p>
      <p>Homeworld: {person.homeworld}</p>
    </div>
  );
}

export default Person;
