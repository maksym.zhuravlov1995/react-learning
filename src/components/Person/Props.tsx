import { PersonType } from './types';

export interface Props {
  person: PersonType;
}
