import React from 'react';
import ReactDom from 'react-dom';

import './Modal.scss';

import { Props } from './Props';

const Modal: React.FC<Props> = ({ children }): JSX.Element => {
  return ReactDom.createPortal(
    <div className="modal">
      <div className="modal__overlay"></div>
      <div className="modal__content">
        {children}
      </div>
    </div>,
    document.body
  );
}

export default Modal;
