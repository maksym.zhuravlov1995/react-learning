import { InputType } from '../Input/types';

export interface Props {
  configInputs: InputType[];
  className: string;
  isPopupWaiting: boolean;
  sendDataForm: (arg0: {}, arg1: boolean) => void;
}
