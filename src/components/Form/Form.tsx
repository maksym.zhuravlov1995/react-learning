import React, { useState, useEffect } from 'react';
import { CircularProgress } from '@material-ui/core';

import './Form.scss';
import Input from '../../components/Input';

import { InputType } from '../Input/types';
import { Props } from './Props';

const Form: React.FC<Props> = ({
  configInputs, className, isPopupWaiting, sendDataForm
}): JSX.Element => {
  const [ formValues, setFormValues ] = useState<any>({});
  const [ errors, setErrors ] = useState<any>({});

  useEffect(() => {
    const fields: any = {};
    const fieldsErrors: any = {};

    for (const input of configInputs) {
      fields[input.name] = '';

      if (input.isRequired) {
        fieldsErrors[input.name] = '';
      }
    }

    if (localStorage.getItem('form-values')) {
      const storedFormValues = JSON.parse(localStorage.getItem('form-values')!);

      for (const value of Object.keys(storedFormValues)) {
        fields[value] = storedFormValues[value];
      }
    }

    setFormValues(fields);
    setErrors(fieldsErrors);
  }, []);

  const isFormValid = (): boolean => {
    const inputErrors: any = {};

    for (const error of Object.keys(errors)) {
      inputErrors[error] = false;

      if (!formValues[error].trim()) {
        inputErrors[error] = true;
      }
    }

    if (formValues.repeatPassword && formValues.password !== formValues.repeatPassword) {
      inputErrors.repeatPassword = true;
    }

    if (Object.values(inputErrors).includes(true)) {
      setErrors(inputErrors);

      return false;
    }

    return true;
  };

  return (
    <form action="#" className={`${className}__form`}>
      {configInputs.map((configInput: InputType) => (
        <Input
          className={className}
          configInput={configInput}
          formValues={formValues}
          setFormValues={setFormValues}
          errors={errors}
          setErrors={setErrors}
          key={configInput.id}
          name={configInput.name}
          value={formValues[configInput.name]}
        />
      ))}
      <button
        className="signup__submit"
        type="button"
        onClick={() => sendDataForm(formValues, isFormValid())}
      >
        Log in
      </button>
      {isPopupWaiting && (
        <CircularProgress />
      )}
    </form>
  );
}

export default Form;
