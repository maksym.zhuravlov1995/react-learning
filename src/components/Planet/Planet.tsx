import React from 'react';

import './Planet.scss';

import { Props } from './Props';

const Planet: React.FC<Props> = ({ planet }): JSX.Element => {
  return (
    <div className="planet" key={planet.name}>
      <p className="planet__name">{planet.name}</p>
      <p>Diameter: {planet.diameter}</p>
      <p>Rotation period: {planet.rotation_period}</p>
      <p>Orbital period: {planet.orbital_period}</p>
      <p>Gravity: {planet.gravity}</p>
      <p>Climate: {planet.climate}</p>
      <p>Terrain: {planet.terrain}</p>
      <p>Surface water: {planet.surface_water}</p>
      <p>Population: {planet.population}</p>
    </div>
  )
}

export default Planet;
