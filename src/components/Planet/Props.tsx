import { PlanetType } from './types';

export interface Props {
  planet: PlanetType;
}
