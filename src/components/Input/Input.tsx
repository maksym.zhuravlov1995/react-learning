import React from 'react';

import './Input.scss';

import { Props } from './Props';

const Input: React.FC<Props> = ({
  className, configInput, formValues, setFormValues, errors, setErrors, name, value
}) => {

  const handleInputChange = (event: any): void => {
    const { name } = event.target;

    setFormValues({
      ...formValues,
      [name]: event.target.value
    });

    setErrors({ ...errors, [name]: false });
  };

  return (
    <div className={`${className}__field`} key={configInput.id}>
      <label htmlFor={configInput.id} className={`${className}__label`}>
        {configInput.label} {configInput.isRequired && (`*`)}
      </label>
      <input
        type={configInput.type}
        className={[`${className}__input`, errors[configInput.name] && `${className}__input--error`].filter(Boolean).join(' ')}
        id={configInput.id}
        name={name}
        value={value}
        onChange={handleInputChange}
      />
      {errors[configInput.name] && (
        <p className={`${className}__error-message`}>
          {configInput.errorMessage}
        </p>
      )}
    </div>
  );
}

export default Input;
