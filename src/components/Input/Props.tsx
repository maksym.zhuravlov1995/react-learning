import { InputType } from './types';

export interface Props {
  className: string;
  configInput: InputType;
  formValues: any;
  setFormValues: any;
  errors: any;
  setErrors: any;
  name: string;
  value: string;
}
