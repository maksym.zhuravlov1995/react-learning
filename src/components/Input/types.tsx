export type InputType = {
  label: string;
  type: string;
  id: string;
  name: string;
  errorMessage: string | null;
  isRequired: boolean;
}
