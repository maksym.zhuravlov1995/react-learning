import React from 'react';
import { NavLink } from 'react-router-dom';

import './Header.scss';

const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <div className="header__inner">
          <nav className="header__nav">
            <NavLink
              to="/"
              className="header__link"
              activeClassName="header__link--active"
              exact
            >
              Home
            </NavLink>
            <NavLink
              to="/people"
              className="header__link"
              activeClassName="header__link--active"
            >
              People
            </NavLink>
            <NavLink
              to="/planets"
              className="header__link"
              activeClassName="header__link--active"
            >
              Planets
            </NavLink>
          </nav>
        </div>
      </div>
    </header>
  );
}

export default Header;
