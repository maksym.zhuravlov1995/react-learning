import React from 'react';
import { Route, Switch } from 'react-router-dom';

import './global.scss';
// images
// types

import PrivateRouter from './PrivateRouter';
import Signup from './pages/Signup';
import Signin from './pages/Signin';
import Home from './pages/Home';
import People from './pages/People';
import Planets from './pages/Planets';

const Router: React.FC = (): JSX.Element => {
  return (
    <Switch>
      <Route path="/signup" exact component={Signup} />
      <Route path="/signin" exact component={Signin} />
      <PrivateRouter path="/" exact component={Home} />
      <PrivateRouter path="/people" exact component={People} />
      <PrivateRouter path="/planets" exact component={Planets} />
      <Route path="/" exact component={Home} />
      {/* <Route render={(props: any) => (
        isLogged ? <Home /> : <Redirect to="/signup" />
      )} /> */}
      {/* <Route path="/people" exact component={People} />
      <Route path="/planets" exact component={Planets} /> */}
    </Switch>
  );
}

export default Router;
